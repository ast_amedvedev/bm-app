const queryInfo = {
    active: true,
    currentWindow: true
};

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.status == 'complete') {
        chrome.tabs.query(queryInfo, tabs => {
            let tab = tabs[0];

            let isAjaxify = localStorage['isAjaxify'] ? JSON.parse(localStorage['isAjaxify']) : true;
            let isBeatify = localStorage['isBeatify'] ? JSON.parse(localStorage['isBeatify']) : true;
            let message = {
                isAjaxify,
                isBeatify
            };

            chrome.tabs.sendMessage(tab.id, message);
        });
    }
});
