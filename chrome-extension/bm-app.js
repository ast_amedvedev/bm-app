var navUrl = window.location.origin.concat(dwUrlPath);
var csrfToken = BMCSRFInjector.tokenValue();
var beatifyLink = jQuery("#bmBeatifyCss");
var mainTable = jQuery('table.main');
var loader = jQuery("#loader");
var footer = jQuery('.footer');
var headerRow = mainTable.find('#bm_header_row');
var contentRow = mainTable.find('#bm_content_column');

var leftNavigation = jQuery('<aside/>', {
    id: 'left-navigation'
});

leftNavigation.append(jQuery('<header/>'));
leftNavigation.append(jQuery('<nav/>'));

mainTable.before(leftNavigation);
mainTable.wrap('<main>');
mainTable.after(loader);

leftNavigation.find('header')
    .append(headerRow.find('a.header__logo').clone())
    .append(headerRow.find('.header__global-navigation-bar').clone());

function removeViolatedStyles() {
    jQuery('style').map(function (index, item) {
        if (item.innerHTML.indexOf('<!--') === 0) {
            jQuery(item).remove();
        }
    });
}

function insertMenuGroup(group, content) {
    var link = group.find('.menu-overview-link');
    link.clone().appendTo(leftNavigation.find('nav'));

    if (content) {
        leftNavigation.find('nav').append(minifyHtml(content));
    }
}

function moveStorefrontLink() {
    var globalNavbar = leftNavigation.find('.header__global-navigation-bar ul');
    var storefrontItem = globalNavbar.find('li:last-child').clone();
    storefrontItem.appendTo(globalNavbar);
    storefrontLink = headerRow.find('.menu.storelink').find('.goto-storefront').clone();
    storefrontLink.find('.menu-overview-link-icon').attr('class', 'headermenu-link-icon');
    storefrontLink.find('.menu-overview-link-text').remove();
    storefrontLink
        .attr('href', storefrontLink.attr('onmouseover').toString().match(/status='(.*)'/)[1])
        .attr('class', 'headermenu storefront')
        .attr('target', '_blank')
        .removeAttr('onmouseover')
        .removeAttr('onmouseout');
    storefrontItem.html(storefrontLink);
}

function renderNavbar() {
    console.log('lokkek');
    var urls = [
        'SiteNavigationBar-CreateSiteNavigationBarBM',
        'SiteNavigationBar-CreateAdminNavigationBarBM'
    ];

    Promise.all(urls.map(function (url) {
        return fetch(navUrl + url, {
            credentials: "same-origin"
        }).then(function (response) {
            return response.text();
        });
    })).then(function (texts) {
        insertMenuGroup(headerRow.find('.bm-dropdown-menu.merchant-tools'), texts[0]);
        insertMenuGroup(headerRow.find('.bm-dropdown-menu.admin'), texts[1]);
    }).then(function () {
        moveStorefrontLink();

        var menusHead = leftNavigation.find('.bm-menu-head');
        menusHead.append(jQuery('<a/>', { href: "#", class: "plus" }).append('(+)'));
        menusHead.append(jQuery('<a/>', { href: "#", class: "minus" }).append('(-)'));

        leftNavigation.find('.overview_item_bm').on('click', function() {
            leftNavigation.find('.overview_item_bm').removeClass('selected');
            jQuery(this).addClass('selected');
        });

        leftNavigation.find('a.minus, a.plus').on('click', function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().toggleClass('expand');
        });

        leftNavigation.find('a.plus').on('click', function (e) {
            leftNavigation.find('nav').scrollTop(jQuery(e.target).parent().position().top);
        });

        ajaxify();
        mainTable.show();
    });
}

function reloadScriptBMNavigation() {
    // window.BMSiteNavigation.loaded = false;
    var script = jQuery('script[src*="bm-site-nav.js"]')[0];
    var url = script.src.split('?')[0];
    script.remove();
    var updateScript = jQuery('<script/>', {
        src: url + '?' + (new Date()).getTime()
    });
    document.head.append(updateScript[0]);
}

function beatifyRun() {
    var header = leftNavigation.find('header');
    header.find('a.header__logo').after(headerRow.find('.header__site-switcher'));
    beatifyLink.removeAttr('disabled');
    leftNavigation.show();
    headerRow.hide();
    footer.hide();
}

function beatifyStop() {
    var header = headerRow.find('header');
    header.find('a.header__logo').after(leftNavigation.find('.header__site-switcher'));
    beatifyLink.attr('disabled', 'disabled');
    // reloadScriptBMNavigation();
    leftNavigation.hide();
    headerRow.show();
    footer.show();
}

function renderContent(data, callback) {
    document.getElementById('bm_content_column').innerHTML = data;
    loader.fadeOut();
}

function renderLoader() {
    loader.fadeIn();
}

function minifyHtml(html) {
    return html.replace(/>\s+</g, '><');
}

function findCSRF(data) {
    var matched = data.match(/'csrf_token',\n'(.*)'/);
    return matched ? matched[1] : null;
}

function ajaxResponse(body, response) {
    var responseType = response.headers.get('Content-Type');
    // terminate before angularjs break page 
    if (body.indexOf('angular') !== -1) {
        location.reload();
        return;
    }

    // fix pagination buttons closing slash in first tag of pair
    body = body.replace(/perm_not_disabled"\/>/g, 'perm_not_disabled">');

    // remove all bad commented styles tags
    body = body.replace(/<style([\S\s]*?)><!--([\S\s]*?)--><\/style>/ig, '');

    // try to catch csrf token in response
    csrfToken = findCSRF(body) || csrfToken;

    var parser = new DOMParser();
    parsedDocument = parser.parseFromString(body, 'text/html');
    // we need flux/redux object LoadedContent with events
    bmContentLoaded = parsedDocument.getElementById('bm_content_column');

    if (bmContentLoaded && bmContentLoaded.children.length) {
        bmContentLoaded = bmContentLoaded.children[0].innerHTML;
        // remove all spaces
        bmContentLoaded = minifyHtml(bmContentLoaded);
    } else {
        // use response as html page when is no content column
        bmContentLoaded = body;
    }

    // fix js troubles in BM
    var pattern = /else return true;\nelse return true;/gm;
    bmContentLoaded = bmContentLoaded.replace(pattern, 'else return true;');

    // fix flatten forms with controls out of within
    var loadedContentDom = parser.parseFromString(bmContentLoaded, 'text/html');
    var hasFlattenFormTableCell = loadedContentDom.querySelector('form + td');
        
    if(hasFlattenFormTableCell) {
        var form = hasFlattenFormTableCell.previousElementSibling;
        var tableRow = form.parentElement;
        var tBody = tableRow.parentElement;
        var table = tBody.parentElement;
        var superParent = table.parentElement;
        superParent.insertBefore(form, table);
        form.appendChild(form.nextElementSibling);
        bmContentLoaded = loadedContentDom.body.innerHTML;
    }
    
    // wrap text content for better readable
    if (responseType == 'text/plain') {
        var wrapper = document.createElement('div');
        wrapper.setAttribute('style', 'white-space: pre;');
        wrapper.innerHTML = bmContentLoaded;
        bmContentLoaded = wrapper.outerHTML;
    }

    renderContent(bmContentLoaded);

    ajaxify();
}

function linkHandler(e, urlAction) {
    if (urlAction.toString().indexOf('about:blank#') !== -1) {
        return true;
    }

    renderLoader();

    fetch(urlAction, {
        credentials: "same-origin"
    })
        .then(function (response) {
            return response.text().then(function (body) {
                ajaxResponse(body, response);
            });
        })
        .catch(console.error);
}

function formHandler(target, urlAction) {
    var form = jQuery(target);
    var body;
    var headers = {};

    if (form.attr('enctype') == "multipart/form-data") {
        body = new FormData(target);
    } else {
        var data = form.serializeArray();
        var submitedControl = form.find("button:focus");

        if (submitedControl.length) {
            data.push({
                name: submitedControl.attr("name"),
                value: submitedControl.attr("value")
            });
        }

        var index = data.findIndex(function (item) {
            return item.name == 'csrf_token';
        });

        data[index] = {
            name: "csrf_token",
            value: csrfToken
        };

        body = jQuery.param(data);

        urlAction.searchParams.set('csrf_token', csrfToken);

        headers = {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        };
    }

    renderLoader();

    fetch(urlAction.toString(), {
        credentials: "same-origin",
        method: "POST",
        headers: headers,
        body: body
    })
        .then(function (response) {
            return response.text().then(function (body) {
                ajaxResponse(body, response);
            });
        })
        .catch(console.error);
}

function ajaxifyHandler(e) {
    e.preventDefault();
    var urlAction;
    var nodeName = e.target.nodeName.toLowerCase();
    
    if (nodeName == 'form') {
        urlAction = new URL(e.target.getAttribute('action'));
        formHandler(e.target, urlAction);
    } else {
        var href = e.currentTarget.getAttribute('href');
        var title = e.currentTarget.getAttribute('title');
        var target = e.currentTarget.getAttribute('target');
        title = title || 'inner text';

        // fix for links with relative site url, 
        // in issue it is link for download file
        if(href.indexOf('/') === 0) {
            window.location = href;
            return true;
        }

        history.pushState(null, title, href);

        urlAction = new URL(href);
        var route = getUrlRoute(urlAction);

        // exclude routes
        if (route == 'ViewSiteImpex-Status' || route == 'ViewApplication-DisplayWelcomePage' || route == 'ViewChannelCatalogList_52-Start') {
            window.location = urlAction.toString();
            return;
        }

        linkHandler(e.currentTarget, urlAction);
    }
}

function getUrlRoute(url) {
    return url.pathname.split('/').slice(-1)[0];
}

function ajaxify() {
    console.log('ajaxify');
    ajaxifyStop();

    if (window.angular) {
        return false;
    }

    var route = getUrlRoute(window.location);
    var routePair = route.split('-');
    var controller = routePair[0];
    var action = routePair[1];

    if (controller == 'ViewSiteImpex') {
        return false;
    }

    if (isAjaxify) {
        console.log('ajaxifyRun');
        ajaxifyRun();
    }
}

function ajaxifyRun() {
    contentRow.find('a[target!="_blank"]').on('click', ajaxifyHandler);
    leftNavigation.find('a[target!="_blank"]').on('click', ajaxifyHandler);
    jQuery(document).on('submit', ajaxifyHandler);
}

function ajaxifyStop() {
    contentRow.find('a[target!="_blank"]').off('click', ajaxifyHandler);
    leftNavigation.find('a[target!="_blank"]').off('click', ajaxifyHandler);
    jQuery(document).off('submit', ajaxifyHandler);
}

removeViolatedStyles();
renderNavbar();

document.addEventListener('enableAjaxify', function () {
    console.log('enableAjaxify');
    isAjaxify = true;
    ajaxify();
});

document.addEventListener('enableBeatify', function () {
    console.log('enableBeatify');
    isBeatify = true;
    beatifyRun();
});

document.addEventListener('disableAjaxify', function () {
    console.log('disableAjaxify');
    isAjaxify = false;
    ajaxify();
});

document.addEventListener('disableBeatify', function () {
    console.log('disableBeatify');
    isBeatify = false;
    beatifyStop();
});

var evt = document.createEvent('Event');
evt.initEvent('pageReady', true, false);
document.dispatchEvent(evt);

