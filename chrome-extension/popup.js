const queryInfo = {
    active: true,
    currentWindow: true
};

const ajaxifyCheckbox = document.getElementById('ajaxify');
const beatifyCheckbox = document.getElementById('beatify');

const isAjaxify = localStorage['isAjaxify'] ? JSON.parse(localStorage['isAjaxify']) : true;
const isBeatify = localStorage['isBeatify'] ? JSON.parse(localStorage['isBeatify']) : true;

ajaxifyCheckbox.checked = isAjaxify;
beatifyCheckbox.checked = isBeatify;

ajaxifyCheckbox.addEventListener('change', e => {
    let isAjaxify = e.target.checked;
    localStorage.setItem('isAjaxify', isAjaxify);

    chrome.tabs.query(queryInfo, tabs => {
        let tab = tabs[0];
        let message = {
            isAjaxify
        };
        chrome.tabs.sendMessage(tab.id, message);
    });
}, false);

beatifyCheckbox.addEventListener('change', e => {
    let isBeatify = e.target.checked;
    localStorage.setItem('isBeatify', isBeatify);

    chrome.tabs.query(queryInfo, tabs => {
        let tab = tabs[0];
        let message = {
            isBeatify
        };
        chrome.tabs.sendMessage(tab.id, message);
    });
}, false);

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.status == 'complete') {
        chrome.tabs.query(queryInfo, tabs => {
            let tab = tabs[0];

            let isAjaxify = localStorage['isAjaxify'] ? JSON.parse(localStorage['isAjaxify']) : true;
            let isBeatify = localStorage['isBeatify'] ? JSON.parse(localStorage['isBeatify']) : true;
            let message = {
                isAjaxify,
                isBeatify
            };
            
            chrome.tabs.sendMessage(tab.id, message);
        });
    }
});