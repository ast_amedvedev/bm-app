const initUrl = chrome.extension.getURL('init.js');
const bmAppUrl = chrome.extension.getURL('bm-app.js');
const styleCssUrl = chrome.extension.getURL('styles.css');
const loaderUrl = chrome.extension.getURL('loader.gif');

function createNode(name, attributes = {}) {
    let node = document.createElement(name);
    Object.keys(attributes).forEach(item => {
        node.setAttribute(item, attributes[item]);
    });
    return node;
}

function injectNode(name, attributes) {
    let node = createNode(name, attributes);
    document.body.appendChild(node);
}

function scriptInject(attributes) {
    injectNode('script', attributes);
}

function linkInject(attributes) {
    injectNode('link', attributes);
}

document.documentElement.appendChild(createNode('script', {
    src: initUrl
}));

const loader = createNode('div', {
    id: 'loader'
});
// const loaderImg = createNode('img', {
//     src: loaderUrl
// });
const loaderImg = createNode('div');
loader.appendChild(loaderImg);

document.addEventListener('DOMContentLoaded', () => {
    linkInject({
        href: styleCssUrl,
        id: 'bmBeatifyCss',
        rel: 'stylesheet'
    });
    scriptInject({
        src: bmAppUrl
    });
    document.body.appendChild(loader);
}, false);

function lolkek(message, option) {
    if (typeof message[`is${option}`] !== 'undefined') {
        const evt = document.createEvent('Event');

        if (message[`is${option}`]) {
            evt.initEvent(`enable${option}`, true, false);
        } else {
            evt.initEvent(`disable${option}`, true, false);
        }

        document.dispatchEvent(evt);
    }
}

document.addEventListener('pageReady', () => {
    chrome.runtime.onMessage.addListener((message, sender, response) => {
        lolkek(message, 'Ajaxify');
        lolkek(message, 'Beatify');
    });
});