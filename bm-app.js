function renderStyles() {
    var style = document.createElement("style");
    style.innerHTML = `
        *, *::before, *::after {
            box-sizing: border-box !important;
        }

        html, body {
            height: 100%;
        }

        body {
            padding-bottom: 0;
        }

        main {
            height: 100%;
            width: 85%;
            margin-left: 15%;
            overflow-y: scroll;
        }

        aside#left-navigation {
            width: 15%;
            position: fixed;
            top:0;
            left:0;
            bottom: 0;
            overflow-y: scroll;
            margin-top: 122px;
        }

        aside#left-navigation header {
            position: fixed;
            width: 15%;
            top: 0;
        }

        div.bm-menu-column {
            padding: 0 20px 0 10px;
            display: block;
            max-width: none;
            color: #fff;
            //font-size: 16px;
        }

        div.bm-menu-item {
            font-size: 14px;
            line-height: 1.5;
        }

        div.bm-menu-section {
            margin-bottom: 10px;
        }

        div.menu_items_bm {
            padding-top: 10px;
            display: none;
        }
        
        div.bm-menu-head {
            font-size: 12px;
        }

        div.bm-menu-section,
        div.bm-menu-head,
        div.bm-menu-head a,
        div a.bm-menu-item {
            position: static;
        }
       
        .leftmenu_sub_bm.expand .menu_items_bm {
            display: block;
            padding-left: 20px;
        }

        .plus, .minus {
            font-size: 16px;
            font-weight: normal;
        }

        .leftmenu_sub_bm .plus,
        .leftmenu_sub_bm.expand .minus
        {
            display: inline;
        }

        .leftmenu_sub_bm.expand .plus,
        .leftmenu_sub_bm .minus
        {
            display: none;
        }

        a.header__logo {
            width: 25%;
            height: auto;
            float: none;
        }

        span.menu-overview-link {
            margin-bottom: 10px;
            height: 50px;
        }

        span.menu-overview-link-text {
            line-height: 50px;
        }

        span.menu-overview-link, 
        span.menu-overview-link a {
            font-size: 20px;
            background-color: #f5f5f5;
        }

        div.header__site-switcher__selection {
            float: left;
        }

        div.header__site-switcher {
            float: right;
            width: 75%;
        }

        div.header__global-navigation-bar {
            width: 100%;
            border: 1px solid #54585a;
            border-left: none;
        }

        div#bm_content_column {
            padding: 25px;
        }
    `;

    document.head.append(style);
}

var csrfToken = BMCSRFInjector.tokenValue();

function renderNavbar() {
    var mainTable = jQuery('table.main');
    mainTable.wrap('<main>');
    var headerRow = mainTable.find('#bm_header_row');
    var leftNavigation = jQuery('<aside/>', {
        id: 'left-navigation'
    });
    jQuery('main').after(leftNavigation);

    var navHeader = jQuery('<header/>')
        .append(headerRow.find('a.header__logo'))
        .append(headerRow.find('.header__site-switcher--Sandbox'))
        .append(headerRow.find('.header__global-navigation-bar'));

    leftNavigation.append(navHeader);

    var extractColumns = function(data) {
        var columns = data.find('.bm-menu-column');
        var previousTitle;
        columns.each((c, item) => {
            item.innerHTML = item.innerHTML.replace(/>\s+</g, '><');
            var title = jQuery(item).closest('.bm-dropdown-menu').find('.menu-overview-link');
            if (title.find('.menu-overview-link-text').text() != previousTitle) {
                title.find('.menu-overview-link-icon').remove();
                leftNavigation.append(title);
            }
            previousTitle = title;

            item.innerHTML.length ? leftNavigation.append(item) : null
        });
    }

    jQuery.ajax({
        url: 'https://dev04-web-burpee.demandware.net/on/demandware.store/Sites-Site/default/SiteNavigationBar-CreateSiteNavigationBarBM',
        type: 'GET',
        success: function(response) {
            extractColumns(jQuery(response).find('.bm-menu-column'));    
        }
    });

    jQuery.ajax({
        url: 'https://dev04-web-burpee.demandware.net/on/demandware.store/Sites-Site/default/SiteNavigationBar-CreateAdminNavigationBarBM',
        type: 'GET',
        success: function(response) {
            extractColumns(jQuery(response).find('.bm-menu-column'));  
        }
    });

    // var columns = headerRow.find('.bm-menu-column');
    // var previousTitle;
    // columns.each((c, item) => {
    //     item.innerHTML = item.innerHTML.replace(/>\s+</g, '><');
    //     var title = jQuery(item).closest('.bm-dropdown-menu').find('.menu-overview-link');
    //     if (title.find('.menu-overview-link-text').text() != previousTitle) {
    //         title.find('.menu-overview-link-icon').remove();
    //         leftNavigation.append(title);
    //     }
    //     previousTitle = title;

    //     item.innerHTML.length ? leftNavigation.append(item) : null
    // });

    // jQuery('.bm-menu-head').append(jQuery('<a/>', {href: "#", class: "plus"}).append('(+)'));
    // jQuery('.bm-menu-head').append(jQuery('<a/>', {href: "#", class: "minus"}).append('(-)'));

    // jQuery('a.minus, a.plus').on('click', function(e) {
    //     e.preventDefault();
    //     jQuery(this).parent().parent().toggleClass('expand');
    // });

    // jQuery('a.plus').on('click', function(e) {
    //     var objDiv = document.getElementById("left-navigation");
    //     objDiv.scrollTop = objDiv.scrollHeight;
    // });

    jQuery('.footer').hide();
    headerRow.hide();
}

function renderContent(data) {
    jQuery('#bm_content_column').parent().html(data);
}

function renderLoader() {
    renderContent('<p id="bm_content_column">Loading...</p>');
}

function findCSRF(data) {
    var matched = data.match(/'csrf_token',\n'(.*)'/);
    return matched ? matched[1] : null;
}

function ajaxResponse(response) {
    // terminate before angularjs break page 
    if (response.indexOf('angular') !== -1) {
        location.reload();
        return;
    }

    // fix pagination buttons closing slash in first tag of pair
    response = response.replace(/perm_not_disabled"\/>/g, 'perm_not_disabled">');

    // try to catch csrf token in response
    csrfToken = findCSRF(response) || csrfToken;

    bmContentLoaded = jQuery(response).find('#bm_content_column');

    // use response as html page when is no content column
    if (bmContentLoaded.length) {
        bmContentLoaded = bmContentLoaded.parent().html();
        // remove all spaces
        bmContentLoaded = bmContentLoaded.replace(/>\s+</g, '><');
    } else {
        bmContentLoaded = response;
    }

    // fix js troubles in BM
    bmContentLoaded = bmContentLoaded.replace(/else return true;\nelse return true;/gm, 'else return true;');
    renderContent(bmContentLoaded);
    ajaxify();
}

function linkHandler(e) {
    e.preventDefault();

    if(jQuery(e.target).attr("href") == "#") {
        return true;
    }

    jQuery.ajax({
        url: this.href,
        type: 'GET',
        success: ajaxResponse,
        beforeSend: renderLoader
    });
}

function formHandler(e) {
    e.preventDefault();

    var form = jQuery(e.target);
    var data = form.serializeArray();
    var submitedControl = form.find("button:focus");

    if (submitedControl.length) {
        data.push({
            name: submitedControl.attr("name"),
            value: submitedControl.attr("value")
        });
    }

    var index = data.findIndex(item => item.name == 'csrf_token');
    data[index] = { name: "csrf_token", value: csrfToken };
    data = jQuery.param(data);

    url = new URL(jQuery(form).attr('action'));
    url.searchParams.set('csrf_token', csrfToken);

    jQuery.ajax({
        url: url.toString(),
        data: data,
        type: 'POST',
        success: ajaxResponse,
        beforeSend: renderLoader
    });
}

function ajaxify() {
    jQuery('a').off('click', linkHandler);
    jQuery('a').on('click', linkHandler);
    jQuery(document).off('submit', formHandler);
    jQuery(document).on('submit', formHandler);
}

renderNavbar();
renderStyles();
ajaxify();